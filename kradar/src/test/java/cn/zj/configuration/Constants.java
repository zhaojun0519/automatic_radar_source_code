package cn.zj.configuration;

import java.io.File;

//此类用来存储测试框架中用到的各种参数配置，如数据文件路径、sheet名称和sheet中使用的列好等
public class Constants {
    //测试数据相关常量设定
//    File file = new File(this.getClass().getResource("关键字驱动测试用例.xlsx").getPath());
    public static final String Path_ExcelFile = "关键字驱动测试用例.xlsx";
    public static final String path_test = "F:\\1-ZJ\\tools\\IdeaProjects\\kradar\\src\\test\\resources\\关键字驱动测试用例.xlsx";

    public static final String Path_ConfigurationFile = "kradar\\src\\test\\resources\\objectMap.properties";

    //测试数据sheet中的列号常量设定
    //第一列用0表示，测试用例序号列
    public static final int Col_TestCaseID = 0;
    //第四列用3表示，关键字列
    public static final int Col_KeyWordAction = 3;
    //第五列用4表示，操作元素的定位表达式列
    public static final int Col_LocatorExpression = 4;
    //第六列用5表示，操作值列
    public static final int Col_ActionValue =5;
    //第七列用6小时，测试结果列
    public static final int Col_TestStepTestResult = 6;

    //测试集合Sheet中的列号常量设定
    public static final int Col_RunFlag =2;
    //测试用例Sheet名称的常量设定;
    public static final String Sheet_TestSteps = "正常登陆";

    //测试用例集sheet的常量设定
    public static final String Sheet_TestSuite = "测试用例集合";


    //测试集合sheet中的列号常量设定，测试用例执行结果列
    public static final int Col_TestSuiteTestResult =3;


}
