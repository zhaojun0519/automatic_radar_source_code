package cn.zj.testScript;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import cn.zj.configuration.KeyWordsAction;
import cn.zj.util.ToolUtil;
import com.sun.org.apache.xpath.internal.compiler.Keywords;
import org.apache.log4j.lf5.util.ResourceUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.FieldDocument;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import static cn.zj.util.KeyBoardUtil.*;
import static cn.zj.util.WaitUtil.*;


import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TestSendMailWithAttachment {
    WebDriver driver;
    String baseUrl;
    @Test
    public void testSendMailAttachment() throws Exception {


        driver.get("http://192.168.0.165:8080/#/login");
        WebElement userName = driver.findElement(By.xpath("//input[@placeholder='请输入用户名']"));
        WebElement passWord = driver.findElement(By.xpath("//input[@placeholder='请输入密码']"));
        WebElement loginButton = driver.findElement(By.xpath("//button[@class='el-button el-button--primary']"));
        userName.clear();
        userName.sendKeys("终端公司");
        passWord.clear();
        passWord.sendKeys("123456");
        loginButton.click();
//        waitWebElement(driver,"//*[@id=\"app\"]/div/div[2]/div/div[3]/div/div[2]/div");
        KeyWordsAction.driver = driver;
        //广告投放
        driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[1]/ul/li[5]/div")).click();
//        sleep(3000);
        //广告计划管理
        driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[1]/ul/li[5]/ul/li[1]")).click();
//        sleep(15000);
        //新建广告计划

//        KeyWordsAction.waitForElementEnabled("advPlanManage.addAdvPlan","");
        driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[3]/div/div[1]/div/div/div/div/div[1]/div[2]/span")).click();
//        sleep(5000);

        driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[3]/div/div[1]/div/div/div/div[2]/div[1]/div[1]/div[1]/form/div[3]/button")).click();
//        sleep(1000);

        driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[3]/div/div[1]/div/div/div/div[2]/div[1]/div[1]/div[2]/form/div[4]/button")).click();
//        sleep(1000);

        driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[3]/div/div[1]/div/div/div/div[2]/div[1]/div[1]/div[3]/form/div[1]/div/div[3]/div/div/div")).click();
//        sleep(1000);

        KeyWordsAction.uploadPhoto("","C:\\Users\\user\\Desktop\\测试图片\\test.jpg");



//        //点击数据管理平台链接
//        WebElement databutton = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='广告投放平台'])[1]/following::span[1]"));
//        sleep(15000);
//        databutton.click();
//        // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | win_ser_1 | ]]
//        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
//        sleep(10000);
//        Set<String> allWindowsHandles = driver.getWindowHandles();
//        String parentHandles = driver.getWindowHandle();
//
//            for(String windowHandle : allWindowsHandles){
//                if (!windowHandle.equals(parentHandles)){
//                    driver.switchTo().window(windowHandle);
//                    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
//                    WebElement d = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[3]/ul/li[2]/ul/li[2]"));
//                    d.click();
//                    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
//                    WebElement dd = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div/div[2]/div[1]/div[3]/table/tbody/tr[2]/td[4]/div/button"));
//                    dd.click();
//
//                    WebElement ddd = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div/div[2]/div[1]/div/div[1]/div[1]/div/input"));
//                    ToolUtil.setAttribute(driver,ddd,"spellcheck","true");
//                    ddd.sendKeys("2018-10-21 00:00");
//
//                    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
//                    sleep(10000);
//                    WebElement dddd = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div/div[2]/div[1]/div/button[1]"));
//                    dddd.click();
//


                    //复选框选择
//                    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='全选/反选'])[1]/following::span[2]")).click();
//                    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
//
//                    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='全选/反选'])[1]/following::span[5]")).click();
                }

//            }










    @BeforeTest
    public void beforeMethord() throws InterruptedException{
        //设定chrome驱动路径
        String fileName = this.getClass().getClassLoader().getResource("chromedriver.exe").getPath();
        System.setProperty("webdriver.chrome.driver", fileName);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
    }

}
