package cn.zj.testScript;

import cn.zj.configuration.Constants;
import cn.zj.configuration.KeyWordsAction;
import cn.zj.util.ExcelUtil;
import cn.zj.util.Log;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.apache.log4j.xml.DOMConfigurator;


import java.io.InputStream;
import java.lang.reflect.Method;

public class TestSuiteByExcel {
    public static Method method[];
    public static String keyword;
    public static String locatorExpression;
    public static String value;
    public static KeyWordsAction keyWordsAction;
    public static int testStep;
    public static int testLastStep;
    public static String testCaseID;
    public static String testCaseRunFlag;
    public static boolean testResult;

    @Test
    public void testTestSuite() throws Exception{
        keyWordsAction = new KeyWordsAction();
        method = keyWordsAction.getClass().getMethods();

//        String excelFilePath = Constants.Path_ExcelFile;
//        ExcelUtil.setExcelFile(excelFilePath);
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(Constants.Path_ExcelFile);
        ExcelUtil.setExcelFile(is ,Constants.Sheet_TestSteps);

        int testCasesCount = ExcelUtil.getRowCount(Constants.Sheet_TestSuite);
        //for循环，执行所有标记为y的测试用例
        for(int testCaseNo = 1;testCaseNo<=testCasesCount;testCaseNo++){
            //读取“测试用例集合”sheet中每行的测试用例序号
            testCaseID = ExcelUtil.getCellData(Constants.Sheet_TestSuite,testCaseNo,Constants.Col_TestCaseID);
            //读取“测试用例集合”sheet中每行是否运行列中的值
            testCaseRunFlag = ExcelUtil.getCellData(Constants.Sheet_TestSuite,testCaseNo,Constants.Col_RunFlag);
            //如何是否运行列中的值是y，则执行测试用例中的所有步骤
            if(testCaseRunFlag.equalsIgnoreCase("y")){
                Log.startTestCase(testCaseID);
                testResult = true;
              testStep = ExcelUtil.getFirstRowContainsTestCaseID(Constants.Sheet_TestSteps,testCaseID,Constants.Col_TestCaseID);
              testLastStep = ExcelUtil.getTestCaseLastStepRow(Constants.Sheet_TestSteps,testCaseID,testStep);

              //遍历测试用例中的所有测试步骤
              for (;testStep <testLastStep;testStep++){
                  //从“正常登陆”sheet中读取关键字和操作值，
                  keyword = ExcelUtil.getCellData(Constants.Sheet_TestSteps,testStep,Constants.Col_KeyWordAction);
                  Log.info("从excel文件读取到的关键字是："+keyword);
                  locatorExpression = ExcelUtil.getCellData(Constants.Sheet_TestSteps,testStep,Constants.Col_LocatorExpression);
                  Log.info("从excel文件读取到的表达式是："+locatorExpression);
                  value = ExcelUtil.getCellData(Constants.Sheet_TestSteps,testStep,Constants.Col_ActionValue);
                  Log.info("从excel文件读取到的操作值是："+value);

                  execute_Actions();
                  if(testResult ==false){
                      ExcelUtil.setCellData(Constants.Sheet_TestSuite,testCaseNo,Constants.Col_TestSuiteTestResult,"测试执行失败");
                      Log.endTestCase(testCaseID);
                      break;
                  }else if(testResult == true){
                      ExcelUtil.setCellData(Constants.Sheet_TestSuite,testCaseNo,Constants.Col_TestSuiteTestResult,"测试执行成功");
                  }

              }


            }
        }

    }

    private static void execute_Actions(){
        try {
            for (int i = 0;i <method.length;i++){
                if (method[i].getName().equals(keyword)){
                    method[i].invoke(keyWordsAction,locatorExpression,value);
                    if(testResult == true){
                        ExcelUtil.setCellData(Constants.Sheet_TestSteps,testStep,Constants.Col_TestStepTestResult,"测试步骤执行成功");
                        break;
                    }else {
                        ExcelUtil.setCellData(Constants.Sheet_TestSteps,testStep,Constants.Col_TestStepTestResult,"测试步骤执行失败");

                        break;
                    }

                }
            }
        }catch (Exception e){
            Assert.fail("执行出现异常，测试用例执行失败");
        }
    }
    @BeforeClass
    public void beforeClass(){
        DOMConfigurator.configure("log4j");
    }
}
