package cn.zj.testScript;

import cn.zj.configuration.Constants;
import cn.zj.configuration.KeyWordsAction;
import cn.zj.util.ExcelUtil;
import org.apache.log4j.lf5.util.ResourceUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.lang.reflect.Method;
//从excel中读取关键字测试数据，并通过java反射机制实现对每个测试步骤的执行
public class TextSendMailWithAttachmentByExcel {
    public static Method method[];
    public static String keyword;
    public static String value;
    public static KeyWordsAction keyWordsAction;


    @Test
    public void testSendMailAttachment() throws Exception{
        keyWordsAction = new KeyWordsAction();
        //使用java反射机制获取keyWordsAction类的所有方法对象
        method = keyWordsAction.getClass().getMethods();
        //定义excel关键文件的路径
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(Constants.Path_ExcelFile);

        ExcelUtil.setExcelFile(is ,Constants.Sheet_TestSteps);

        for(int iRow = 1; iRow <= ExcelUtil.getRowCount(Constants.Sheet_TestSteps);iRow++){
            //读取excel文件中的第4列
            keyword = ExcelUtil.getCellData(iRow,Constants.Col_KeyWordAction);
            //读取excel文件中的第5列
            value = ExcelUtil.getCellData(iRow,Constants.Col_ActionValue);
            execute_Actions();
        }

    }

    private static void execute_Actions() throws Exception{

            for (int i = 0;i <method.length;i++){
                if (method[i].getName().equals(keyword)){
                    method[i].invoke(keyWordsAction,value);
                    break;
                }
            }

    }



}
