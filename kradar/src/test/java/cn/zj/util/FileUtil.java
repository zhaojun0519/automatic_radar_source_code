package cn.zj.util;

import java.io.File;
import java.io.IOException;

public class FileUtil {
    public static boolean createFile(String destFileName){
        File file = new File(destFileName);
        if(file.exists()){
            Log.info("创建"+destFileName+"文件失败，目标文件已存在！");
            return  false;
        }
        if (destFileName.endsWith(file.separator)){
            Log.info("创建"+destFileName+"文件失败，目标文件不能为目录");
            return false;
        }
        if (!file.getParentFile().exists()){
            Log.info("创建"+destFileName+"时失败，目标文件所在目录不存在");
            if(!file.getParentFile().mkdir()){
                Log.info("创建目标文件所在目录失败");
                return false;
            }
        }
        try {
            if (file.createNewFile()){
                Log.info("创建"+destFileName+"文件成功");
                return true;
            }else {
                Log.info("创建"+destFileName+"文件失败");
                return false;
            }
        }catch (IOException e){
            e.printStackTrace();
            Log.info("创建"+destFileName+"文件失败");
            return false;
        }

    }

    public static  boolean createDir(String destDirName){
        File dir = new File(destDirName);
        if (dir.exists()){
            Log.info("创建"+destDirName+"目录失败");
            return false;
        }

        if(dir.mkdirs()){
            Log.info("创建"+destDirName+"目录成功");
            return true;
        }else {
            Log.info("创建"+destDirName+"目录失败");
            return false;
        }
    }
}
