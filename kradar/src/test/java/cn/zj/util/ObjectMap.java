package cn.zj.util;

import org.openqa.selenium.By;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ObjectMap {
    Properties properties;
    public ObjectMap(InputStream inputStream){
        properties = new Properties();
        try {
            InputStream in = inputStream;
            properties.load(in);
            in.close();
        }catch (IOException e){
            System.out.println("读取对象文件出错");
            e.printStackTrace();
        }
    }
    public By getLocator(String ElementNameInpropFile)throws Exception{
        //从属性配置文件中读取对应的配置对象
        String locator = properties.getProperty(ElementNameInpropFile);
         String locatorType = locator.split(">")[0];


         String locatorValue = locator.split(">")[1];
//         locatorValue = new String(locatorValue.getBytes("ISO-8859-1"),"UTF-8");
         System.out.println("获取的定位类型："+locatorType+"\t 获取的定位表达式" +locatorValue);
         //根据locatorType变量值的内容判断返回何种定位方式的By对象
        if(locatorType.toLowerCase().equals("id"))
            return By.id(locatorValue);
        else if(locatorType.toLowerCase().equals("name"))
            return By.name(locatorValue);
        else if ((locatorType.toLowerCase().equals("classname")) || (locatorType.toLowerCase().equals("class")))
            return By.className(locatorValue);
        else if((locatorType.toLowerCase().equals("tagname")) || (locatorType.toLowerCase().equals("tag")))
            return By.tagName(locatorValue);
        else if(locatorType.toLowerCase().equals("linktext"))
            return By.linkText(locatorValue);
        else if(locatorType.toLowerCase().equals("partiallinktext"))
            return By.partialLinkText(locatorValue);
        else if(locatorType.toLowerCase().equals("cssselector"))
            return By.cssSelector(locatorValue);
        else  if(locatorType.toLowerCase().equals("xpath"))
            return By.xpath(locatorValue);
        else
            throw new Exception("输入的locator type 未在程序中被定义："+locatorType);
    }
}
