package cn.zj.util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ToolUtil {
    //修改页面元素属性的封装方法
    public static void setAttribute(WebDriver driver, WebElement element, String attributteName, String value){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].setAttribute(arguments[1],arguments[2])",element,attributteName,value);
    }
}
