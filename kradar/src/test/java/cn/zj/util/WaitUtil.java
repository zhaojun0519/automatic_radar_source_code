package cn.zj.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.datatransfer.StringSelection;

public class WaitUtil {
    //用户测试执行过程中暂停程序执行的休眠方法
    public static void sleep(long millisecond){
        try {
            Thread.sleep(millisecond);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //显式等待，使用Xpath定位字符串
    public static void waitWebElement(WebDriver driver, String xpathExpression){
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathExpression)));
    }

    //显式等待，使用By对象定位，支持更多定位表达式
    public static void waitWebElement(WebDriver driver,By by){
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }
}
